/**
 * Created by lindsayadland on 1/10/16.
 */
"use strict";
var express = require("express");
var app = express();
app.set('view engine', 'pug');
app.set('x-powered-by', true);
app.locals.pretty = true;
var listen_port = process.env.PORT || 1337;
//login
app.get("/", function (req, res) {
    res.render('index', {title: 'markt', authenticated: false});
});
//authenticated app
app.get("/dashboard", function (req, res) {
    res.render('dashboard', {title: 'markt - dashboard', authenticated: true});
});
app.get("/products", function (req, res) {
    res.render('products', {title: 'markt - products', authenticated: true});
});
app.get("/orders", function (req, res) {
    res.render('orders', {title: 'markt - orders', authenticated: true});
});
app.get("/support", function (req, res) {
    res.render('support', {title: 'markt - support', authenticated: true});
});
app.get("/settings", function (req, res) {
    res.render('settings', {title: 'markt - settings', authenticated: true});
});
//static routes
app.use("/styles", express.static('styles'));
app.use("/scripts", express.static('scripts'));
app.use("/images", express.static('images'));
app.use("/extras", express.static('bower_components'));
app.use("/controllers", express.static('controllers'));
app.listen(listen_port, function () {
    console.log("Markt is available on port " + listen_port);
});
//# sourceMappingURL=router.js.map